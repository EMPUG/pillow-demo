## Python Image Processing with Pillow

These scripts and images were used in a demonstration of Pillow given to the
Eastern Michigan Python User Group ([EMPUG](https://empug.org)) on 2019-09-21.

See the [Pillow](https://pillow.readthedocs.io/en/stable/) documentation for how
to install and use the Pillow package.

### PIL vs. Pillow

Pillow is a friendly fork of PIL (the Python Imaging Library). PIL was last
updated in 2009, and does not support Python 3.

### File formats

According to the Pillow documentation, "The Python Imaging Library supports a
wide variety of raster file formats. Over 30 different file formats can be
identified and read by the library. Write support is less extensive, but most
common interchange and presentation formats are supported."

See https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html

By default, when saving an image to a file, Pillow will use the image format
corresponding the file extension.

### Creating thumbnails

*thumbnail.py* creates a thumbnail of an existing image, with the maximum
dimension specified on the command line.

### Rotating an image

*rotate.py* rotates an image counter-clockwise by a specified number of degrees.

### Reading EXIF data

*exif.py* displays any EXIF (Exchangeable Image File Format) data included in
the image. https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html is
a good reference for the EXIF tags and their possible values.

### Applying a Sepia filter

*sepia.py* creates a sepia version of an image, using the first algorithm
presented in [Yábir G. Benchakhtir's blog post](https://yabirgb.com/blog/creating-a-sepia-filter-with-python/).
In his post, Yábir goes on to develop more efficient algorithms.

### Notes

The *leopard.jpg* photo was created by Gwen Weustink on [Unsplash](https://unsplash.com)
and was resized to a smaller image, so it can be converted more quickly to sepia
during the demo.
