"""
Read an image file and dispay its EXIF data.

Usage:
    python exif.py IMAGE_FILENAME
"""

import sys
import os
import json

from PIL import Image
from PIL import ExifTags


def main():
    # Read the image filename from the command line.
    try:
        fname = sys.argv[1]
    except (OSError, IOError, ValueError, IndexError) as e:
        print(f'Usage: python {sys.argv[0]} IMAGE_FILENAME')
        sys.exit(1)

    # Make sure the image file exists.
    if not os.path.exists(fname):
        print(f'Error: {fname} does not exist')
        sys.exit(1)

    # Open the image and read the raw EXIF data.
    img = Image.open(fname)
    raw_exif = img._getexif()
    print('Raw EXIF Data:')
    print(raw_exif)
    print()

    # Create a dict using EXIF tag names and exclude bytes objects.
    exif_data = {
        ExifTags.TAGS[k]: v
        for k, v in raw_exif.items()
        if k in ExifTags.TAGS and not isinstance(v, bytes)
    }

    print('EXIF data with tag names:')
    print(json.dumps(exif_data, sort_keys=True, indent=4))

if __name__ == '__main__':
    main()
