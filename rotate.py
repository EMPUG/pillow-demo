"""
Rotate an image counter-clockwise by the specified number of degrees.

Usage:
    python rotate.py IMAGE_FILENAME NEW_FILENAME ANGLE
"""

import sys
import os

from PIL import Image


def main():
    # Read the image filename and angle to rotate from the command line.
    try:
        fname = sys.argv[1]
        new_fname = sys.argv[2]
        angle = int(sys.argv[3])
    except (OSError, IOError, ValueError, IndexError) as e:
        print(f'Usage: python {sys.argv[0]} IMAGE_FILENAME NEW_FILENAME ANGLE')
        sys.exit(1)

    # Make sure the image file exists.
    if not os.path.exists(fname):
        print(f'Error: {fname} does not exist')
        sys.exit(1)

    # Open the image, rotate it and save it.
    img = Image.open(fname)
    new_img = img.rotate(angle, expand=True)
    new_img.save(new_fname)
    print(f'Created {new_fname}')


if __name__ == '__main__':
    main()
