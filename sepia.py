"""
Create a sepia version of an image, using the algorithm provided in
https://yabirgb.com/blog/creating-a-sepia-filter-with-python/

Usage:
    python sepia.py IMAGE_FILENAME NEW_FILENAME
"""

import sys
import os

from PIL import Image


def main():
    # Read the image filename from the command line.
    try:
        fname = sys.argv[1]
        new_fname = sys.argv[2]
    except (OSError, IOError, ValueError, IndexError) as e:
        print(f'Usage: python {sys.argv[0]} IMAGE_FILENAME NEW_FILENAME')
        sys.exit(1)

    # Make sure the image file exists.
    if not os.path.exists(fname):
        print(f'Error: {fname} does not exist')
        sys.exit(1)

    # Open the image and read it into a pixel map.
    img = Image.open(fname)
    width, height = img.size
    pixels = img.load()

    # Apply the transformation to each pixel.
    for py in range(height):
        for px in range(width):
            r, g, b = img.getpixel((px, py))

            tr = int(0.393 * r + 0.769 * g + 0.189 * b)
            tg = int(0.349 * r + 0.686 * g + 0.168 * b)
            tb = int(0.272 * r + 0.534 * g + 0.131 * b)

            if tr > 255:
                tr = 255

            if tg > 255:
                tg = 255

            if tb > 255:
                tb = 255

            pixels[px, py] = (tr, tg, tb)

    img.save(new_fname)


if __name__ == '__main__':
    main()
