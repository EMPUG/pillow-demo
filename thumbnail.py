"""
Create a thumbnail of an image, with the maximum dimension given on the
command line.

Usage:
    python thumbnail.py IMAGE_FILENAME THUMBNAIL_SIZE
"""

import sys
import os

from PIL import Image


def main():
    # Read the image filename and thumbnail dimension from the command line.
    try:
        fname = sys.argv[1]
        new_size = (int(sys.argv[2]), int(sys.argv[2]))
    except (OSError, IOError, ValueError, IndexError) as e:
        print(f'Usage: python {sys.argv[0]} IMAGE_FILENAME THUMBNAIL_SIZE')
        sys.exit(1)

    # Make sure the image file exists.
    if not os.path.exists(fname):
        print(f'Error: {fname} does not exist')
        sys.exit(1)

    # Create the filename for the thumbnail.
    new_fname = os.path.splitext(fname)[0] + '.thumb' + os.path.splitext(fname)[1]

    # Open the image and display its size.
    img = Image.open(fname)
    print(f'{fname}: {img.size[0]} x {img.size[1]} pixels')

    # Create and save the thumbnail.
    new_img = img.copy()
    new_img.thumbnail(new_size)
    print(f'{new_fname}: {new_img.size[0]} x {new_img.size[1]} pixels')
    new_img.save(new_fname)


if __name__ == '__main__':
    main()
